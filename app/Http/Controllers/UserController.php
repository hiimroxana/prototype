<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function Store(Request $request)
    {

        $user = User::where('id',auth()->user()->id)->first();

        $user->full_name = $request->full_name;
        $user->filled_for_visa = $request->filled_for_visa;
        $user->save();

        return response()->json(
            [
                'success' => true,
                'message' => 'Your data has been saved.'
            ]
     );

    }

    public function show()
    {
        return view('show', ['users' => User::all()]);
    }

}
