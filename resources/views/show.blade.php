@extends('layouts.app')

@section('content')
<div class="m-5">
<h3 class="mb-3">List of all users & form submissions</h3>
<table class="table">
    <thead>
      <tr>
        <th scope="col">Username</th>
        <th scope="col">Full Name</th>
        <th scope="col">Filled For Visa</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <th scope="row">{{ $user->name }}</th>
        <td>{{ $user->full_name }}</td>
        <td>{{ $user->filled_for_visa }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection('content')
