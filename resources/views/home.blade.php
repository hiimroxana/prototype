<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Symply') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Symply') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto"></ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('home') }}">Fill form</a>
                                    <a class="dropdown-item" href="{{ route('show') }}">View all data</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Welcome! Please fill out the following form:</div>
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="container">
                                <form action="" method="POST">
                                    <div>
                                    <p id="message"></p>
                                    <p>What is your full name?</p>
                                    <input class="form-control" placeholder="Name" id="full_name" name="full_name">
                                     <p class="name_error_text"></p>
                                    </div>
                                <div class="mb-5">
                                <p class="pt-lg-5">How many times have you filed for a US visa?</p>
                                    <div class="form-check form-check-inline">
                                       <input class="form-check-input" type="radio" id="visa_0" name="filled_for_visa" value="Never"  checked>
                                       <label class="form-check-label" for="visa_0">
                                       Never
                                       </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="visa_1" name="filled_for_visa" value="Once">
                                        <label class="form-check-label" for="visa_1">
                                        Once
                                        </label>
                                     </div>
                                     <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="visa_more" name="filled_for_visa" value="More Then Once">
                                        <label class="form-check-label" for="visa_more">
                                        More Then Once
                                        </label>
                                     </div>
                                </div>
                                  <input type="button" value="Submit" id="submit"  class="btn btn-outline-primary btn-block col-auto">

                                  <p style="color:green" id="success"></p>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>

<script>
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

     //Validation before submit
    $(".name_error_text").hide();

    var error_name = false;

    $("#full_name").focusout(function () {
       check_name();
    });


   function check_name(){
    $("#message").hide();
    var name_length = $("#full_name").val().length;
    if(name_length < 1){
         $(".name_error_text").html("Name must be filled out");
         $(".name_error_text").show().addClass("error");
         $("#message").hide();
         error_name = true;
      }else{
         $(".name_error_text").hide();
      }
    }

    //Submit Data
    $("#submit").click(function(e){

        e.preventDefault();

        var full_name = $("#full_name").val();
        var filled_for_visa = $('input[name="filled_for_visa"]:checked').val();
        var url = '{{ url('addanswers') }}';

    if (full_name.length < 1 ){

        check_name();

    }else {

        $.ajax({
           url:url,
           method:'POST',
           data:{
               full_name:full_name,
                  filled_for_visa:filled_for_visa
                },
           success:function(response){
              $("#success").html(response.message)
           },
        });
      }
   });

   });
</script>
</html>
